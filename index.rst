.. index::
   pair: Fediverse; RSS
   ! really simple syndication


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/tuto-rss/rss.xml>`_

.. _tuto_fediverse:

=====================================
**RSS (really simple syndication)**
=====================================

- https://fr.wikipedia.org/wiki/RSS
- https://gofoss.net/foss-apps/#rss-online-radio

.. toctree::
   :maxdepth: 5

   news/news
   find/find
   firefox/firefox
   people/people
   readers/readers
   examples/examples
