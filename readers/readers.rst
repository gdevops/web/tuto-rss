.. index::
   pair: Readers; RSS

.. _rss_readers:

===========================================
RSS readers
===========================================

- https://gofoss.net/foss-apps/#rss-online-radio

.. toctree::
   :maxdepth: 3

   feedbot/feedbot
   flux/flux
