.. index::
   pair: Flux ; RSS Reader

.. _flux_rss_readers:

===========================================
Flux RSS reader
===========================================


Et notre service Flux, lecteur #RSS libre et open source (la solution
utilisée étant l'excellent #FreshRSS), vient de passer la petite barre
des 200 utilisateurs et utilisatrices.

C'est encore peu mais ça fait plaisir de voir que c'est utile aussi pour vous.

Si vous ne connaissez pas, voici la page de description du service : https://www.zaclys.com/flux/
