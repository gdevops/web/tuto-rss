.. index::
   pair: Styling an Django RSS Feed ; Julian-Samuel Gebühr (2024-04-16)

.. _gebuhr_2024_04_18:

======================================================================
2024-04-16 **Styling an Django RSS Feed** by Julian-Samuel Gebühr
======================================================================

- https://hyteck.de/post/django-rss/
- https://darekkay.com/blog/rss-styling/


Introduction
================

RSS is amazing !

While not everyone thinks that, most people that understand RSS, like it.

This presents a problem, as most people don’t have chance to learn about it.
Unless there is a person in the community that doesn’t shut up about how
great RSS is (maybe that person is you), they might not even know what it is,
let alone use it.

One big reason for this is, that when you click an link to an RSS feed you
download a strange file that most people don’t know how to deal with.
Maybe your browser is nice and renders some XML which is also not meant for human
consumption.

Wouldn’t it be better if people clicked on the RSS link and were greeted by  a text explaining RSS and how to use it ?
==========================================================================================================================

Wouldn’t it be better if people clicked on the RSS link and were greeted by
a text explaining RSS and how to use it ?
And if the site would still be a valid RSS feed?

Luckily you don’t have to imagine that - it’s possible ! You can even try
it on this blog by clicking the RSS link in the menu (direct link).


Doing this has not been my idea.

Darek Kay described this in the blog post Style your RSS feed and I just copied
most of their work !

This was fairly easy for this Hugo blog and is available in my fork of the
hugo-nederburg-theme.

However, in a Django project it get’s a bit more complicated.

Let me explain.

The Problem
=====================

Django has the great Syndication feed framework, a high level framework to
create RSS and Atom Feeds.

This is great as we only need a few lines of code to create a feed.

Here is an example from notfellchen.org that list animals that are in search
for a new home.

People should be able to follow the RSS feed to see new adoption notices.

So lets do it


.. code-block:: python

    # in src/fellchennasen/feeds.py
    from django.contrib.syndication.views import Feed

    from .models import AdoptionNotice

    class LatestAdoptionNoticesFeed(Feed):
        title = "Notfellchen"
        link = "/rss/"
        description = "Updates zu neuen Vermittlungen."

        def items(self):
            return AdoptionNotice.objects.order_by("-created_at")[:5]

        def item_title(self, item):
            return item.name

        def item_description(self, item):
            return item.description

.. code-block:: python

    # in src/fellchennasen/urls.py
    urlpatterns = [
        path("", views.index, name="index"),
        path("rss/", LatestAdoptionNoticesFeed(), name="rss"), # <--- Added
        ...

**Wait that’s it ? Yeah! We have a working RSS feed**.
And it was very convenient, Django allows us to create by just pointing it
to the right model and fields we want to display.

But here is the problem: How do we style this? We can’t just add a link to
a stylesheet here.

The solution
==================

- https://darekkay.com/blog/rss-styling/

First we need to add our styling files.

I’ll not go into detail how they work her, just `refer Darek’s blog post https://darekkay.com/blog/rss-styling/ <https://darekkay.com/blog/rss-styling/>`_
for that.

In Django we add them to our static files

- static/rss.xsl will be adjusted based on `this file v <https://hyteck.de/post/django-rss/rss.xsl>`_.
  It is responsible for creating a html rendering of your XML file
- static/css/rss-styles you can drop in `this file https://hyteck.de/post/django-rss/rss-styles.css <https://hyteck.de/post/django-rss/rss-styles.css>`_,
  which is a basic CSS file you can edit to your liking.

**After that comes the hard part**.

How do tweak this wonderfully simple Feed class to include a link to our style sheet ?

I first thought “that must be easy, just follow the docs on custom feed
generators and add a root element.

Something like this:

.. code-block:: python

    class FormattedFeed(Rss201rev2Feed):

        def add_root_elements(self, handler):
            super().add_root_elements(handler)
            # We want <?xml-stylesheet href="/static/rss.xsl" type="text/xsl"?>
            handler.addQuickElement("?xml-stylesheet", f'href="{static("rss.xsl")}"')

    class LatestAdoptionNoticesFeed(Feed):
        feed_type = FormattedFeed
        title = "Notfellchen"
        ...

Looks good. Let’s try. Oh no what is this ?

Yes, we can’t correctly close this tag.

There is (to my knowledge) no easy way to do this.

So let’s take the hard road an implement a custom write function.

In the following the write function will be copied from django.utils.feedgenerator.RssFeed.
We make two important changes to the class:

1. Changing the content type from content_type = "application/rss+xml; charset=utf-8"
   to content_type = "text/rss+xml; charset=utf-8.
   This will make a browser display the content rather than opening it in a app.
2. Adding our xml-stylsheet information. This is done in the write() function
   with this line

::

    handler._write(f'<?xml-stylesheet href="{static("rss.xsl")}" type="text/xsl"?>')

Putting it all together we still have a relativly simple solution with only
the necessary adjustments. Here is the full feeds.py:

.. code-block::

    from django.contrib.syndication.views import Feed
    from django.utils.feedgenerator import Rss201rev2Feed
    from django.templatetags.static import static
    from django.utils.xmlutils import SimplerXMLGenerator

    from .models import AdoptionNotice


    class FormattedFeed(Rss201rev2Feed):
        content_type = "text/xml; charset=utf-8"
        def write(self, outfile, encoding):
            handler = SimplerXMLGenerator(outfile, encoding, short_empty_elements=True)
            handler.startDocument()
            handler._write(f'<?xml-stylesheet href="{static("rss.xsl")}" type="text/xsl"?>')
            handler.startElement("rss", self.rss_attributes())
            handler.startElement("channel", self.root_attributes())
            self.add_root_elements(handler)
            self.write_items(handler)
            self.endChannelElement(handler)
            handler.endElement("rss")


    class LatestAdoptionNoticesFeed(Feed):
        feed_type = FormattedFeed
        title = "Notfellchen"
        link = "/rss/"
        description = "Updates zu neuen Vermittlungen."

        def items(self):
            return AdoptionNotice.objects.order_by("-created_at")[:5]

        def item_title(self, item):
            return item.name

        def item_description(self, item):
            return item.description

And finally we have what we want !

A RSS feed displayed in the browser, with beginner-friendly explanation and
still completely spec-compliant.

Outlook
==============

Now you may recognize I’m not a frontend person.

The style could be prettier and provide a better overview.
But I’d argue the improvement is immense and might help a user to get started
with RSS.

There are still a couple things to improve:

- Translation: The current text is only displayed in english
- The rss.xsl file has a hard-coded link to the css stylesheet in it::

    <link rel="stylesheet" type="text/css" href="/static/fellchensammlung/css/rss-styles.css"/>

Both can be solved by templating the rss.xsl instead of serving it as static file.

So have fun playing around !

If you have created or found a nice-looking RSS feed let me know.
Let’s keep RSS alive and thriving!

