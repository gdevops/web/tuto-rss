.. index::
   ! RSS is dead lol

.. _rss_dead_lol:

========================================================
2024-03-01 **Explore RSS feeds in your neighbourhood**
========================================================

- https://mastodon.social/@paulcuth.rss
- https://mastodon.social/@paulcuth/112020399228840502
- https://rss-is-dead.lol/
- https://mastodon.social/@paulcuth
- https://enhance.dev/
- https://fosstodon.org/@enhance_dev


Mastodon announce
======================

- https://mastodon.social/@paulcuth/112020399228840502

I made a thing to help me find RSS feeds, and I really liked it.
So I made it prettier so you can use it too, if RSS feeds are your thing.

As it turns out, in my corner of the fediverse there’s a ton of them…


2024-03-29
============

- https://mastodon.social/@paulcuth/112178886374464145

You can now generate an OPML file of all your connections' feeds on
https://rss-is-dead.lol, so you can open them in your fav feed reader 🙌
