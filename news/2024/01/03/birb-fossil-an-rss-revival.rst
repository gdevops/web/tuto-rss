.. index::
   pair: Fossil ; 2024-01-03

.. _kellog_2024_01_03:

===============================================================
2023-01-03 **Birb + Fossil: An RSS Revival ?** by Tim Kellogg
===============================================================

- https://timkellogg.me/blog/2024/01/03/birb
- https://hachyderm.io/@kellogh/111607714159954053
- https://framapiaf.org/@pvergain/111702083725058062

.. tags:: Fossil

Introduction
=============

A few days ago, @twilliability announced Birb, a Mastodon bot where you 
can send it a URL of any RSS feed, Atom feed, podcast, Substack, etc. 
and it’ll create a Mastodon account for it that you can follow. 

This effectively meshes social media and the blogosphere. 

This is great! 

But Mastodon has been notorious for sticking with chronologically-ordered 
timelines, so unless you have time to look at every single post, you’ll 
likely miss something.

Enter fossil
------------

I announced it before New Years. It’s a Mastodon client I made that 
allows experimenting with timeline algorithms. 

Unlike a full Mastodon server, it don’t handle any kind of firehose of 
posts, it merely reformats my home timeline in a way that helps me find 
the interesting stuff and ignore everything else. 

Right now, it groups posts together based on similarity and generates a label.

I have a lot of ideas for how to format a timeline, but frankly, I’m not 
sure they’re good ideas. 

It’s hard to know without trying them out. In the last week, I’ve begun 
pivoting fossil to be more extensible, via plugins so that you can build 
your own timeline algorithm or customize the view, without having to 
clone my repo or send pull requests. 

Hacking is great! We should make hacking even easier!

**So between Birb & Fossil, it seems like we’re seeing an RSS revival**.
