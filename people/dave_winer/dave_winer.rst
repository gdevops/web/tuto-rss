.. index::
   ! Dave Winer

.. _dave_winer:

===========================================
Dave Winer
===========================================

- https://en.wikipedia.org/wiki/Dave_Winer
- https://mastodon.social/@davew
- https://mastodon.social/@davew.rss



I used to be Mastodon on Compuserve CB radio in the early 80s
=================================================================

- https://mastodon.social/@davew/109350821782877682

I used to be Mastodon on Compuserve CB radio in the early 80s.

Now I work on FeedLand and Drummer, outliners and feeds.

And 2-Way RSS.

Lots of wonderful stuff already working, more on the way. #introduction


Annonce
========

- https://indieweb.social/@tchambers/109350202681733549

::

    Hi everyone: the "godfather" of #RSS and #blogging and #podcasting, Dave Winer, is now on #Mastodon.

    Definitely recommend everyone welcome him: @davew

    He is doing fascinating work continuing to evolve the integration
    RSS and the Fediverse even further.
    And I constantly watch what he is creating with #FeedLand. 👀
